function create(x,y)
    foodType = rand() % 2 + 1
    entity = getNewEntity("item", x, y, "lunch" .. foodType)
    removeEntityFunction(entity, "action")
    addEntityFunction(entity, "action", "void")
    return entity
end

function action(entity)
    player = getGlobalValue("player")
    if getEntityRenderFlip(player) == 1 then
        createEntity("lunchStuff", "tray", foodType, getEntityX(entity), getEntityY(entity), -150 - rand() % 50 + getEntityDeltaX(player) / 2, -200 - rand() % 50)
        createEntity("lunchStuff", "food", foodType, getEntityX(entity), getEntityY(entity), -150 - rand() % 50 + getEntityDeltaX(player) / 2, -200 - rand() % 50)
    else
        createEntity("lunchStuff", "tray", foodType, getEntityX(entity), getEntityY(entity), 150 + rand() % 50 + getEntityDeltaX(player) / 2, -200 - rand() % 50)
        createEntity("lunchStuff", "food", foodType, getEntityX(entity), getEntityY(entity), 150 + rand() % 50 + getEntityDeltaX(player) / 2, -200 - rand() % 50)
    end
    setEntityValue(player, "holdingItem", getNullEntity())    
    deleteEntity(entity)
    print("action")
end
