function create(item, foodType, x, y, deltaX, deltaY)
    if item == "food" then
        texture = "food" .. foodType
    else
        texture = "tray"
    end
    sizeMultiplier = getGlobalValue("sizeMultiplier")
    entity = getNewEntity("item", x / sizeMultiplier, y / sizeMultiplier, texture)
    setEntityDeltaX(entity, deltaX)
    setEntityDeltaY(entity, deltaY)
    print("created")
    return entity
end
